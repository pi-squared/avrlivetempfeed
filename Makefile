build:
	docker build --no-cache -t nginx .
 
run:
	docker run -d -p 4001:80 --name livetempfeed ubuntu

clean:
	docker stop livetempfeed && docker rm livetempfeed -v