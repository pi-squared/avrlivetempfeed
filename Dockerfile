FROM registry.gitlab.com/pi-squared/simple-apache-php-container

COPY ./src/public /var/www/html

ENV TIMEZONE=Europe/Athens      \
    HEADER_TITLE="Live Temperature Feed"    \
    PHP_DEBUGGING=0             \
    FOOTER_TITLE=""    \
    ANALYTICS_ID=""