<?php

$config = [
      'timezone' =>  getenv(TIMEZONE),
      'version' => '',
      'header_title' => getenv(HEADER_TITLE),
      'footer_title' => getenv(FOOTER_TITLE),
      'developer_info' => 'kachatzis@ece.auth.gr',
      'db_host'=> getenv(DB_HOST),
      'db_user'=> getenv(DB_USER),
      'db_pass'=> getenv(DB_PASS),
      'db_database'=> getenv(DB_DATABASE),
      'analytics_id'=> getenv(ANALYTICS_ID)
    ];

 ?>
