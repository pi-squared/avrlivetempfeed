<?php require_once 'public_header.php'; ?>



<a href="/?do=exit">Exit</a>

<!-- Selection -->
<div class="row">
  <div class="col-12">
    <div class="card">

      <?php if(!isset($_GET['code'])) { ?>
      <form id="clear_form" method="GET" action=""></form>

      <form id="search_form" method="GET" action="">
          
        <div class="row">

          <!-- Left Col -->
          <div class="col-md-6">
              <input class="form-control form-control-line" placeholder="Code" type="text" name="code"
                <?php if(isset($_GET['code'])){ echo 'value="'.$_GET['string_search'].'"' ;} ?>>
          </div>

          <div class="col-md-4">
            <select name="mode" class="form-control form-control-line">
            <option value="a" <?php if(isset($_GET['mode'])&&($_GET['mode']=='all')){echo ' selected ';}?>>All</option>
            <option value="7d" <?php if(isset($_GET['mode'])&&($_GET['mode']=='7d')){echo ' selected ';}?>>Last 7 Days</option>
            <option value="24h" <?php if(isset($_GET['mode'])&&($_GET['mode']=='24h')){echo ' selected ';}?>>Last 24 Hours</option>
            </select>
          </div>

        </div>
 
        <br>

        <div class="row">
        
          <div class="col-md-12">
            <button class="btn btn-success" action="submit" form="search_form">View Feed</button>
          </div>
        </div>

      </form>
      <?php } else { ?>
        <a href="/feed?do=back">Use another code</a>
      <?php } ?>


    </div>
  </div>
</div>

<!-- Feed -->
<?php if(isset($_GET['code'])){ ?>

<div class="row">
  <div class="col-12">
    <div class="card">

    

      <?php

        require_once 'controllers/RecData.php';

        $recData = new RecData();

        $data = $recData->getCode( $_GET['code'] , $_GET['mode'] );

        if ($data == null){
          echo 'No Data!';
        } else {
          $delim_data = explode(';', str_replace( '],', '', str_replace('[', '', str_replace('],[', ';', $data) ) ) );
          $data = [];
          foreach( $delim_data as $key=>$row ){
            $row_e = explode(',', $row);
            $data[$key] = ['date'=>$row_e[0], 'value'=>$row_e[1]];
          }

          require_once 'utils/LineChart.php';

          $chart = new LineChart();
          $chart->set_name('Temperature');
          $chart->set_y_name('value');
          $chart->set_y_title('Temperature');
          $chart->set_x_name('date');
          $chart->set_type('datetime');
          $chart->set_data(
            $data
          );
          $chart->draw();
        }

      ?>

    </div>
  </div>
</div>

<?php } ?>

<?php require_once 'footer.php'; ?>
