<?php require_once 'public_header.php'; ?>

<div class="row">
  <div class="col-12">
    <div class="card">

      <a href="/feed"><h2>Live Temperature Feed APP</h2></a>
      <a href="/feed">Enter</a>

      <br><br>
      <p>This App contains live data from temperature sensors, deployed on simple AVR-based circuits. Use the code of a device to access the live feed <a href="/feed">here</a>.</p> 
    
    </div>
  </div>
</div>

<?php require_once 'footer.php'; ?>
