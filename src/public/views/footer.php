<?php include 'utils/Config.php'; ?>

</div>
<!-- End Container fluid  -->
<!-- footer -->
<footer class="footer"> © <?php echo date("Y"); ?> <?php echo $config['header_title']; ?></footer>
<!-- End footer -->
</div>
<!-- End Page wrapper  -->
</div>
<!-- End Wrapper -->
<!-- All Jquery -->
<script src="/assets/js/lib/jquery/jquery.min.js"></script>
<!-- Bootstrap tether Core JavaScript -->
<script src="/assets/js/lib/bootstrap/js/popper.min.js"></script>
<script src="/assets/js/lib/bootstrap/js/bootstrap.min.js"></script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $config['analytics_id']; ?>"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', "<?php echo $config['analytics_id']; ?>");
</script>


</body>

</html>
