<?php


class RecData {

    private $db_host;
    private $db_user;
    private $db_pass;
    private $db_database;
    private $link;

    public function __construct(){
        error_reporting(E_ERROR | E_PARSE);
  ini_set('display_errors', 'On');
        include 'utils/Config.php';
        $this->db_host = $config['db_host'];
        $this->db_user = $config['db_user'];
        $this->db_pass = $config['db_pass'];
        $this->db_database = $config['db_database'];

        $this->link = mysqli_connect($this->db_host, $this->db_user, $this->db_pass, $this->db_database);
        mysqli_set_charset($this->link, "utf8");
        if (!$this->link) {
            echo "Error: Unable to connect to MySQL." . PHP_EOL;
            echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
            echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
            exit;
        }
    }
  

    public function getCode( $code , $mode ){ 
        
        $data = "";
        $sql = 'SELECT * FROM rec '. $this->createSearchQuery($code, $mode) . ';' ;
        $result = $this->link->query($sql);
        if ($result->num_rows > 0){
            while($row = $result->fetch_assoc()){
               $data .= '['.$row['datetime'].','.$row['val'].'],';
            }
            return $data;
        }else{
            return null;
        }

        $this->close();

        return null;
        
    }


    public function insert( $value , $code ){

        $value = mysqli_real_escape_string($this->link, $_GET['val']);
        $code = mysqli_real_escape_string($this->link, $_GET['code']);

        $sql = 'INSERT INTO rec (`val`, `code`) VALUES (\''. $value . '\',\'' . $code .'\');' ;
        $result = $this->link->query($sql);
        //var_dump( $sql );
       
        //$result;

        $this->close();

    }


    private function close(){
        mysqli_close($this->link);
    }


    private function createSearchQuery($code, $mode){

        $code =  mysqli_real_escape_string($this->link, $code);
        $mode =  mysqli_real_escape_string($this->link, $mode);

        // Set default Mode
        if($mode!="a" && $mode!="24h" && $mode!="7d"){
            $mode = "24h";
        }

        // Set Code
        $q = ' WHERE code = "'. $code .'" ';

        // Set Timestamp
        switch($mode){
            case "24h":
                $date = date("Y-m-d H:i:s", strtotime('-24 hours', time()));
                $q .= 'AND datetime > "'. $date . '" ' ;
                break;
            case "7d":
                $date = date("Y-m-d H:i:s", strtotime('-168 hours', time()));
                $q .= 'AND datetime > "'. $date . '" ' ;
                break;
        }

        // Apply Limit
        $q .= ' LIMIT 1000 ';
        
        return $q;
    }

}



/*
phpinfo();
*/

?>
